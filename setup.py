from distutils.core import setup

# import subprocess

# print("Tarring templates")
# subprocess.check_call('tar cavf performance_tools/templates/datamill_package.tar.gz -C ./performance_tools/templates/datamill_package . ',
#                       shell = True)

# print("Finished")

setup(
    name='performance_tools',
    version='0.4.3',
    packages=['performance_tools'],
    scripts=['ptools'],
    install_requires=['six'],
    package_data={'performance_tools': [
        'templates/datamill_package.tar.gz',
        'instructionclass.csv'
    ]}
)
