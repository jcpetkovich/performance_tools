NAME = testpackage
VERSION = 0.0.1

# includes and libs
INCS = -I. -I/usr/include
LIBS = -L/usr/lib -L./lib -lc

# flags
CPPFLAGS = -DVERSION=\"${VERSION}\"
CFLAGS = -ggdb -std=c99 -pedantic -Wall -O0 ${INCS} ${CPPFLAGS}
LDFLAGS = -ggdb ${LIBS} -lshared

# compiler and linker
CC = cc

