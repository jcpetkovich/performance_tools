#!/usr/bin/env python2

import os
import sys
import json
import tests.process_results
import sqlite3
import shutil
from performance_tools.result_processor import ResultProcessor, Utils

def process(args):
    [infile, outfile] = args
    process_results.main(infile, outfile)

def process_function_metrics(dictionary,
                             metadata = None,
                             table_name_pattern = None,
                             database = None):

    for metric_name, metric_data in dictionary.iteritems():
        process_data(
            metric_data,
            data_name = metric_name,
            flatten_by = ['function_name', 'function_metric'],
            metadata = metadata,
            database = database,
            table_name_pattern = table_name_pattern
        )

def process_data(dictionary,
                 data_name = None,
                 flatten_by = None,
                 metadata = None,
                 database = None,
                 table_name_pattern = None):

    rows = Utils.dict_flatten(dictionary, flatten_by)
    map(lambda d: d.update(metadata), rows)

    # MAKE CSV WRITER IF NOT ALREADY THERE
    header = sorted(metadata.keys()) + flatten_by
    table_name = Utils.sanitize_table_name(table_name_pattern.format(data_name))
    Utils.create_table(database, table_name, header)

    # WRITE ROWS
    Utils.insert_data(database, table_name, header, rows)

def concatenate(args):
    """Ugh, most of this is necessary because of how inefficient json is
    for storing tables in memory and uncompressed.

    This is ugly as hell, but at least it's fast. I should really
    learn to use databases for this stuff.

    """

    [result_file, job_id, database_file] = args

    # Gather static
    with open(result_file, "r") as job_file:
        job_results = json.load(job_file)

    if job_results == {}:
        return

    database = sqlite3.connect(database_file)

    for commit, data in job_results.iteritems():

        meta = {'job_id': job_id, 'commit': commit}

        process_function_metrics(
            data['static']['function_metrics'],
            metadata = meta,
            database = database,
            table_name_pattern = 'static_function_metrics_{}'
        )

        process_data(
            data['static']['binary_sizes'],
            data_name = 'binary_sizes',
            flatten_by = ['binary', 'binary_size'],
            metadata = meta,
            database = database,
            table_name_pattern = 'static_{}'
        )

        process_data(
            {"symbol_table": data['static']['symbol_table']},
            data_name = 'symbol_table',
            flatten_by = ['metric_name', 'metric_data'],
            metadata = meta,
            database = database,
            table_name_pattern = 'static_{}'
        )

        # Gather benchmark results

        # header = sorted(meta.keys()) + sorted(data['bench_results'].keys())
        # create_table(database, 'bench_results', header)
        # row = data['bench_results']
        # row.update(meta)
        # insert_data(database, 'bench_results', header, [row])

        bench_results = data['bench_results']

        for table_name in ["alg_table", "key_table", "stream_table", "rsa_table"]:
            table = bench_results[table_name]

            db_tablename = 'bench_results_{}'.format(table_name)

            # If table is empty, skip it
            if len(table) < 1:
                continue

            # JSON Normalize table for DB insertion
            table = Utils.table_flatten(table)
            header = sorted(meta.keys()) + sorted(table[0].keys())
            map(lambda row: row.update(meta), table)

            # Insert data into table
            Utils.create_table(database, db_tablename, header)
            Utils.insert_data(database, db_tablename, header, table)

        for results_name in ["powm", "rand"]:
            res = bench_results[results_name]
            db_tablename = 'bench_results_{}'.format(results_name)

            if len(res) < 1:
                continue

            # We have one less result than the length of our results
            num_vals = len(res)
            header = sorted(["V{}".format(num) for num in range(1, num_vals)])
            # Drop first value, which is just a name, normalize result vector
            res = dict(zip(header, res[1:]))

            # Complete header
            header = sorted(meta.keys()) + header

            res.update(meta)

            Utils.create_table(database, db_tablename, header)
            Utils.insert_data(database, db_tablename, header, [res])

        if 'function_metrics' not in data['dynamic']:
            print("Skipping dynamic data for job_id: {job_id}, commit: {commit}\nNo results".format(
                job_id = job_id,
                commit = commit
            ))
            continue

        for metric_name in data['dynamic']['function_metrics'].keys():
            table = data['dynamic']['function_metrics'][metric_name]
            table = Utils.table_flatten(table)
            if len(table) == 0:
                continue
            header = sorted(meta.keys()) + sorted(table[0].keys())
            map(lambda r: r.update(meta), table)
            table_name = 'dynamic_function_metrics_{metric_name}'.format(
                metric_name = metric_name
            )
            table_name = Utils.sanitize_table_name(table_name)
            Utils.create_table(database, table_name, header)
            Utils.insert_data(database, table_name, header, table)

    database.commit()
    database.close()
    return "Success!"

x264_result_processor = ResultProcessor(
    process = process,
    concatenate = concatenate
)

if __name__ == '__main__':
    in_directory = sys.argv[1]
    out_filename = sys.argv[2]

    x264_result_processor.main(in_directory, out_filename, parallel = False)
    shutil.move(os.path.join(in_directory, 'temp.db'), out_filename)
