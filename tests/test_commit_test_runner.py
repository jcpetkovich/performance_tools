from unittest import TestCase
from performance_tools.perf_analyzer import PerfRunner, PerfAnalyzer
from performance_tools.static_analyzer import StaticAnalyzer
from performance_tools.memory_layout_tools import MemoryLayoutTools
from performance_tools.result_processor import Utils, ResultProcessor
from performance_tools.stop_watch import StopWatch

import performance_tools.vcs_project as vcs_project
import os
import subprocess
import re
import shutil
import tempfile
import sqlite3
import sys
from six import iteritems

test_project = './tests/x264'
test_simple_project = './tests/test_project/'
test_results = './tests/experiment_1003_results.tar'
test_results_dir = './tests/experiment_1003'

if not os.path.isdir(test_project):
    subprocess.check_call("cd tests; git clone git://git.videolan.org/x264.git", shell=True)

class FakeBench(vcs_project.VCSProject):

    def __init__(self, repo = None):
        self.setup_called = False
        self.run_called = False
        self.collect_called = False
        self.clean_called = False

        super(FakeBench, self).__init__(repo)

    def __setup__(self):
        self.setup_called = True
        return os.getcwd()

    def __run__(self):
        self.run_called = True
        return os.getcwd()

    def __collect__(self):
        self.collect_called = True
        return os.getcwd()

    def __clean__(self):
        self.clean_called = True
        return os.getcwd()


class TestProject(TestCase):

    def setUp(self):
        self.project = FakeBench(test_project)
        self.project.checkout_commit('master')

    def tearDown(self):
        pass

    def test_project_init(self):
        # Should raise no errors
        vcs_project.Project(test_project)

    def test_get_commits(self, date_from = None, date_to = None):

        self.project.checkout_commit('master')
        expected_commits = ['3a8baa0', '215f2be', '993c81e', '5ee1d03', '7b1301e', '7de9a9a',
                            '68a6268', '5d60b9c', '75d9270', '790c648', '93bf124', '6371c3a',
                            '0046406', 'd2a9d25', '5a76432', 'c3983b8', 'f6e0d28', '5e0fca8',
                            '5ec5c78', 'fd2c4a0', 'faf3dbe', '3269534', '6a82e49', 'b671762',
                            '43ff8f1', 'c2c2a95', '5743b19', '9475e6a', '732b072', 'f2b4f29',
                            'ccda1ba', '8a9608b', '4cf2728', 'b073e87', '9d5ec55', '8eddd52',
                            '05c1646']

        self.assertEqual(expected_commits, self.project.get_commits(date_from="2013 Jan 00:00:00", date_to = "2013 Mar 00:00:00"))

    def test_checkout_commit(self):
        test_commit = '7de9a9a'
        self.project.checkout_commit(test_commit)
        current_commit = self.project.get_current_commit()
        self.assertEqual(current_commit, test_commit)

        test_commit = '3a8baa0'
        self.project.checkout_commit(test_commit)
        current_commit = self.project.get_current_commit()
        self.assertEqual(current_commit, test_commit)

    def test_bench_commit(self):
        self.project.bench_commit('master')
        self.assertEqual(self.project.setup_called, True)
        self.assertEqual(self.project.run_called, True)
        self.assertEqual(self.project.collect_called, True)
        self.assertEqual(self.project.clean_called, True)

    def test_setup(self):
        self.assertEqual(os.path.abspath(test_project), self.project.setup())

    def test_run(self):
        self.assertEqual(os.path.abspath(test_project), self.project.run())

    def test_collect(self):
        self.assertEqual(os.path.abspath(test_project), self.project.collect())

    def test_clean(self):
        self.assertEqual(os.path.abspath(test_project), self.project.clean())


class TestProjectPatching(TestCase):

    def setUp(self):
        self.project = FakeBench(test_simple_project)

    def tearDown(self):
        pass

    def test_patch(self):
        self.project.patch_queue = ['tests/test_project_patch.patch']
        for patch in self.project.patch_queue:
            self.assertTrue(os.path.isabs(patch))
            self.assertTrue(os.path.exists(patch))

        self.project.__call_in_repo__('patch')

        self.maxDiff = None
        patched_file = self.project.call_in_project(lambda: open('testpackage.c', 'r'))

        try:
            self.assertTrue(re.search('whatever, yo', patched_file.read()))
        finally:
            self.project.__call_in_repo__('unpatch')

class TestStaticAnalysis(TestCase):

    class StaticAnalyzerProject(FakeBench):
        def __init__(self, repo = None):
            self.static_analyzer = StaticAnalyzer('./testpackage')
            super(TestStaticAnalysis.StaticAnalyzerProject, self).__init__(repo)

        def __setup__(self):
            self.setup_called = True
            subprocess.check_output("make", shell=True)

        def __clean__(self):
            subprocess.check_call("make clean", shell=True)

    def test_get_linked_libraries(self):

        class StaticAnalyzerInstructionCounter(self.StaticAnalyzerProject):
            def __collect__(self):
                self.collect_called = True
                return self.static_analyzer.get_linked_libraries()

        project = StaticAnalyzerInstructionCounter(test_simple_project)
        project.setup()
        data = project.collect()
        self.assertTrue(all([os.path.exists(path) for path in data]))
        self.assertEqual(len(data), 2)

    def test_count_symbol_instructions(self):

        class StaticAnalyzerInstructionCounter(self.StaticAnalyzerProject):
            def __collect__(self):
                self.collect_called = True
                return self.static_analyzer.count_symbol_instructions()

        project = StaticAnalyzerInstructionCounter(test_simple_project)
        project.setup()
        data = project.collect()

        data_subset = {symbol: data[symbol] for symbol in ['func1', 'func2', 'func3',
                                                           'func4', 'func5', 'main']}
        self.assertEqual(data_subset, {'func1': 18,
                                       'func2': 19,
                                       'func3': 17,
                                       'func4': 18,
                                       'func5': 18,
                                       'main': 22})
        project.clean()

    def test_get_total_instructions(self):

        class StaticAnalyzerTotalInstructionCounter(self.StaticAnalyzerProject):
            def __collect__(self):
                self.collect_called = True
                return self.static_analyzer.count_total_instructions()

        project = StaticAnalyzerTotalInstructionCounter(test_simple_project)
        project.setup()
        data = project.collect()
        self.assertEqual(data, {'libc.so.6': 299477, 'libshared.so': 82, 'testpackage': 201})

        project.clean()

    def test_extract_elf_data(self):

        class StaticAnalyzerSymbolTableExtractor(self.StaticAnalyzerProject):
            def __collect__(self):
                self.collect_called = True
                return self.static_analyzer.symbol_table()

        project = StaticAnalyzerSymbolTableExtractor(test_simple_project)

        project.setup()
        data = project.collect()
        self.assertTrue(re.search(r'\./testpackage', data))
        self.assertTrue(re.search(r'SYMBOL TABLE', data))

        project.clean()

    def test_generate_report(self):

        class StaticAnalyzerSymbolTableExtractor(self.StaticAnalyzerProject):
            def __collect__(self):
                self.collect_called = True
                return self.static_analyzer.generate_report()

        project = StaticAnalyzerSymbolTableExtractor(test_simple_project)

        project.setup()
        data = project.collect()
        self.maxDiff = None
        self.assertEqual(set(data.keys()), set(['./testpackage']))
        self.assertEqual(set(data['./testpackage'].keys()), set(['symbol_instructions', 'total_instructions', 'symbol_table']))
        project.clean()


class TestStopWatch(TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_stop_watch(self):
        result_string = "this worked maybe"
        x = lambda: result_string
        self.stop_watch = StopWatch(x)
        result = self.stop_watch()
        self.assertEqual(result, result_string)
        self.assertEqual(type(self.stop_watch.elapsed), float)


class TestPerfProject(TestCase):

    class PerfProject(FakeBench):
        def __init__(self, repo = None):
            self.perf_runner = PerfRunner()
            super(TestPerfProject.PerfProject, self).__init__(repo)

        def __run__(self):
            self.run_called = True
            self.perf_runner.call_with_perf("ls")

        def __collect__(self):
            self.collect_called = True
            self.perf_runner.archive_perf_results(self.__get_commit__())

    def test_call_with_perf(self):
        project = self.PerfProject(test_project)
        project.run()
        self.assertTrue(project.run_called)
        self.assertTrue(os.path.isfile(os.path.join(test_project, 'perf.data')))

    def test_archive_perf_results(self):

        project = self.PerfProject(test_project)
        project.run()
        project.collect()
        self.assertTrue(project.run_called)
        self.assertTrue(project.collect_called)
        self.assertTrue(os.path.isfile(os.path.join(test_project, 'perf.data.{}'.format(project.commit))))
        self.assertTrue(os.path.isfile(os.path.join(test_project, 'perf.data.{}.tar.bz2'.format(project.commit))))


class TestPerfAnalyzer(TestCase):

    class PerfAnalyzerProject(vcs_project.AutoConfProject):

        def __init__(self, repo):
            self.perf_runner = PerfRunner()
            self.perf_runner.perf_events = ['instructions']
            self.perf_analyzer = PerfAnalyzer()
            super(TestPerfAnalyzer.PerfAnalyzerProject, self).__init__(repo)

        def __run__(self):
            command = "./x264 -o /dev/null {thread_arg} {target_file} > {output_file} 2>&1".format(
                thread_arg = "",
                target_file = "../shortest_sintel_trailer_2k_480p24.y4m",
                output_file = 'out.log'
            )
            self.perf_runner.call_with_perf(command)

        def __collect__(self):
            results = self.perf_analyzer.csv_output()
            self.perf_runner.archive_perf_results(self.__get_commit__())
            # shutil.move('out.log', 'out.log.{}'.format(self.__get_commit__()))
            # shutil.move('x264', 'x264.{}'.format(self.__get_commit__()))
            return results

    def setUp(self):
        self.commit = "efaf0b8"
        self.project = self.PerfAnalyzerProject(test_project)
        self.project.checkout_commit(self.commit)
        self.project.setup()
        self.project.run()

    def test_csv_output(self):
        data = self.project.collect()
        self.assertEqual(set(data.keys()),
                         set(['instructions']))
        for value in data.values():
            self.assertEqual(set(value.keys()), set(['symbol_data', 'header', 'event_count']))

        self.assertTrue(any([any([re.search(pattern, symbol_entry) for pattern in ['refine_subpel',
                                                                                   'x264_mc_chroma_avx2',
                                                                                   'x264_pixel_satd_8x8_avx2',
                                                                                   'x264_pixel_avg2_w8_mmx2',
                                                                                   'x264_pixel_satd_4x4_avx']])
                             for symbol_entry in [item[3] for item in data['instructions']['symbol_data']]]))

    def test_csv_output_bin_post_clean(self):
        bin_path = self.project.call_in_project(lambda: os.path.realpath('x264'))
        shutil.move(bin_path, bin_path + ".bak")
        self.project.clean()
        shutil.move(bin_path + ".bak", bin_path)
        data = self.project.collect()

        self.assertTrue(os.path.exists(bin_path))
        self.assertEqual(set(data.keys()), set(['instructions']))

        for value in data.values():
            self.assertEqual(set(value.keys()), set(['symbol_data', 'header', 'event_count']))

        # Assert that we find NO symbols
        self.assertTrue(any([any([re.search(pattern, symbol_entry)
                                  for pattern in ['refine_subpel',
                                                  'x264_mc_chroma_avx2',
                                                  'x264_print()ixel_satd_8x8_avx2',
                                                  'x264_pixel_avg2_w8_mmx2',
                                                  'x264_pixel_satd_4x4_avx']])
                             for symbol_entry in [item[3]
                                                  for item in data['instructions']['symbol_data']]]))

    def test_csv_output_no_bin(self):
        self.project.clean()
        data = self.project.collect()

        bin_path = self.project.call_in_project(lambda: os.path.realpath('x264'))
        self.assertFalse(os.path.exists(bin_path))
        self.assertEqual(set(data.keys()), set(['instructions']))

        for value in data.values():
            self.assertEqual(set(value.keys()), set(['symbol_data', 'header', 'event_count']))

        # Assert that we find NO symbols
        self.assertFalse(any([any([re.search(pattern, symbol_entry)
                                   for pattern in ['refine_subpel',
                                                   'x264_mc_chroma_avx2',
                                                   'x264_print()ixel_satd_8x8_avx2',
                                                   'x264_pixel_avg2_w8_mmx2',
                                                   'x264_pixel_satd_4x4_avx']])
                              for symbol_entry in [item[3]
                                                   for item in data['instructions']['symbol_data']]]))

class TestMemoryLayout(TestCase):

    def setUp(self):
        self.memory_layout = MemoryLayoutTools()

    def test_pad_memory(self):

        padding_bytes = 2341
        self.memory_layout.pad_memory(padding_bytes)
        padding = os.environ.get('ENV_PADDING')
        self.assertEqual(padding_bytes, len(padding))


class TestResultProcessorUtils(TestCase):

    def test_dict_flatten(self):
        example = {
            "bob": 10,
            "jill": 40,
            "jane": 60,
            "dick": 15
        }
        expected = [
            {"name": "bob", "age": 10},
            {"name": "jill", "age": 40},
            {"name": "jane", "age": 60},
            {"name": "dick", "age": 15}
        ]
        expected = expected
        result = Utils.dict_flatten(example, ['name', 'age'])
        self.assertEqual(sorted(result, key = lambda dic: dic["name"]),
                         sorted(expected, key = lambda dic: dic["name"]))

    def test_table_flatten(self):
        example = [["name", "age"],
                   ["bob", 10],
                   ["jill", 40],
                   ["jane", 60],
                   ["dick"]]

        expected = [
            {"name": "bob", "age": 10},
            {"name": "jill", "age": 40},
            {"name": "jane", "age": 60},
            {"name": "dick", "age": "NA"}
        ]

        expected = expected
        result = Utils.table_flatten(example)
        self.assertEqual(sorted(result, key = lambda dic: dic["name"]),
                         sorted(expected, key = lambda dic: dic["name"]))

    def test_sanitize_table_name(self):
        self.assertEqual(Utils.sanitize_table_name("not-that-bad"), "not_that_bad")

    def prep_test_database(self):
        self.temp_dir = tempfile.mkdtemp()
        self.test_database = sqlite3.connect(os.path.join(self.temp_dir, 'test.db'))

    def test_create_table(self):
        self.prep_test_database()

        Utils.create_table(self.test_database, "test_table", ["one", "two", "three"])

        result = self.test_database.execute("SELECT name FROM main.sqlite_master WHERE type='table'").fetchall()
        self.assertEqual(result, [(u'test_table',)])

        result = self.test_database.execute("SELECT * FROM sqlite_master").fetchall()
        self.assertEqual(result, [(u'table', u'test_table', u'test_table', 2, u"CREATE TABLE test_table ('one', 'two', 'three')")])

    def test_insert_data(self):
        self.prep_test_database()

        data = [
            {"name": "bob", "age": 10},
            {"name": "jill", "age": 40},
            {"name": "jane", "age": 60},
            {"name": "dick", "age": "NA"}
        ]

        header = ["name", "age"]
        Utils.create_table(self.test_database, "test_insert_table", header)
        Utils.insert_data(self.test_database, "test_insert_table", header, data)

        result = self.test_database.execute("SELECT * FROM test_insert_table").fetchall()
        self.assertEqual(result, [(u'bob', 10), (u'jill', 40), (u'jane', 60), (u'dick', u'NA')])


import tests.process_results

def process(args):
    [infile, outfile] = args
    tests.process_results.main(infile, outfile)

import tests.concatenate_results

def concatenate(args):
    tests.concatenate_results.concatenate(args)

class TestResultProcessor(TestCase):

    def setUp(self):
        if not os.path.exists(test_results_dir):
            subprocess.check_call("tar xavf {} -C ./tests".format(test_results), shell=True)

        self.temp_dir = tempfile.mkdtemp()

    def tearDown(self):
        try:
            shutil.rmtree(self.temp_dir)
        except:
            pass

    def test_concatenate(self):

        test_database = os.path.join(self.temp_dir, 'temp.db')

        temp_database = os.path.join(test_results_dir, 'temp.db')
        if os.path.exists(temp_database):
            os.remove(temp_database)

        result_processor = ResultProcessor(
            process = process,
            concatenate = concatenate
        )

        result_processor.main(test_results_dir, test_database, parallel = True)

        self.assertTrue(os.path.exists(test_database))

        shutil.copy(test_database, "/home/jcp/loltest.db")


class TestStaticInstructionAnalyzer(TestCase):

    class StaticAnalyzerProject(FakeBench):
        def __init__(self, repo = None):
            self.static_analyzer = StaticAnalyzer('./testpackage')
            super(TestStaticInstructionAnalyzer.StaticAnalyzerProject, self).__init__(repo)

        def __setup__(self):
            self.setup_called = True
            subprocess.check_output("make", shell=True)

        def __clean__(self):
            subprocess.check_call("make clean", shell=True)

    def test_instruction_classifier(self):
        class StaticAnalyzerInstructionClassifier(self.StaticAnalyzerProject):
            def __collect__(self):
                self.collect_called = True
                return self.static_analyzer.classify_instructions()

        project = StaticAnalyzerInstructionClassifier(test_simple_project)
        project.setup()
        data = project.collect()

        self.assertEqual(data.keys(), [u'func3', u'func2', u'func1', u'main', u'func4', u'__libc_csu_fini', u'_start', u'__libc_csu_init'])
        for f, d in iteritems(data):
            self.assertEqual(d.keys(), ['LOAD', 'FLOATINGPOINT', 'CONTROLFLOW', 'ARITHMETIC', 'INTMUL', 'STORE'])
            if not f == "__libc_csu_fini":
                self.assertTrue(sum(d.values()) > 0)
