#!/usr/bin/env python

import tarfile
import sys
import os
import re
import tempfile
import json
import shutil

def parse_out_log(log):

    log = log.split("\n\n")

    # Tables have irregular patterns so we are forced to handle them individually.
    alg_table = log[0]
    key_table = log[1]
    stream_table = log[2]
    rsa_table = log[3]
    powm = log[4]
    rand = log[5]

    # processing alg_table
    alg_table_header = "algorithm,buf1,buf2,buf3,buf4,buf5"
    alg_table_data = [alg_table_header.split(",")]
    for line in alg_table.split("\n"):
        alg_table_data.append(line.split())

    # processing key_table
    key_table_header = "algorithm,buf1,buf2,buf3"
    key_table_data = [key_table_header.split(",")]
    for line in key_table.split("\n"):
        key_table_data.append(line.split())

    # processing stream_table
    stream_table_lines = stream_table.split("\n")
    stream_table_header = ["algorithm", "ECB/Stream1", "ECB/Stream2", "CBC1",
                           "CBC2", "CFB1", "CFB2", "OFB1", "OFB2", "CTR1",
                           "CTR2", "CCM1", "CCM2", "GCM1", "GCM2"]

    stream_table_lines.pop(0)   # drop garbage line
    stream_table_lines.pop(0)   # drop another garbage line
    stream_table_data = [stream_table_header]
    for line in stream_table_lines:
        stream_table_data.append(line.split())

    # processing rsa_table
    rsa_table_lines = rsa_table.split("\n")
    rsa_table_header = rsa_table_lines.pop(0).split()
    rsa_table_lines.pop(0)      # drop garbage line
    rsa_table_data = [rsa_table_header]
    for line in rsa_table_lines:
        rsa_table_data.append(line.split())

    # processing powm
    powm_data = powm.split()
    # processing rand
    rand_data = rand.split("\n")[0].split()

    results = {
        "alg_table": alg_table_data,
        "key_table": key_table_data,
        "stream_table": stream_table_data,
        "rsa_table": rsa_table_data,
        "powm": powm_data,
        "rand": rand_data
    }

    return results

def main(in_filename, out_filename):

    EXTRACT_PATH = tempfile.mkdtemp()

    OUTPUT_FILENAMES = {
        "out_log": "libgcrypt/out.log.{commit}",
        "perf_data": "libgcrypt/perf_data.{commit}.json",
        "static_data": "libgcrypt/static_data.{commit}.json",
        "benchmark": "libgcrypt/tests/benchmark.{commit}"
    }

    metric_patterns = {}
    metric_patterns['runtime'] = r"runtime=([\d\.]+)"

    tar = tarfile.open(in_filename)
    tar.extractall(path=EXTRACT_PATH)
    tar.close()

    result_files = []
    for dirpath, dirnames, filenames in os.walk(EXTRACT_PATH):
        filenames = map(lambda path: os.path.join(dirpath, path), filenames)
        result_files.extend(filenames)

    commits = filter(lambda path: os.path.basename(path).startswith('out.log'), result_files)
    commits = map(lambda path: re.match(r"out\.log\.(.*)", os.path.basename(path)).group(1), commits)

    results = {}

    try:
        for commit in commits:
            commit_results = {
                "bench_results": {},
                "static": {},
                "dynamic": {}
            }
            out_log = os.path.join(EXTRACT_PATH, OUTPUT_FILENAMES["out_log"].format(commit = commit))
            commit_results["bench_results"] = parse_out_log(open(out_log, "r").read())

            static_data = os.path.join(EXTRACT_PATH, OUTPUT_FILENAMES["static_data"].format(commit = commit))
            static_data = json.load(open(static_data, "r"))
            commit_results["static"]["symbol_table"] = static_data["./tests/benchmark"]["symbol_table"]
            commit_results["static"]["binary_sizes"] = static_data["./tests/benchmark"]["total_instructions"]
            commit_results["static"]["function_metrics"] = {}
            commit_results["static"]["function_metrics"]["instructions"] = static_data["./tests/benchmark"]["symbol_instructions"]

            perf_data = os.path.join(EXTRACT_PATH, OUTPUT_FILENAMES["perf_data"].format(commit = commit))
            perf_data = json.load(open(perf_data, "r"))
            commit_results["dynamic"]["function_metrics"] = {}
            for key in perf_data.keys():

                scaling_factor = perf_data[key]["event_count"]
                res = []
                res.append(perf_data[key]["header"])
                for row in perf_data[key]["symbol_data"]:
                    row[0] = int(float(row[0]) * scaling_factor)
                    res.append(row)

                commit_results["dynamic"]["function_metrics"][key] = res

            results[commit] = commit_results
        json.dump(results, open(out_filename, "w"))

    finally:
        shutil.rmtree(EXTRACT_PATH)

    return (EXTRACT_PATH, out_filename)

if __name__ == '__main__':
    in_filename = sys.argv[1]
    out_filename = sys.argv[2]
    main(in_filename, out_filename)
