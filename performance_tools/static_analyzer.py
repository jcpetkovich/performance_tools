import subprocess
import re
import os
import csv
import performance_tools
from six import iteritems
from performance_tools.utils import split_and_group
from bisect import bisect_left

INS_TABLE = os.path.abspath(
    os.path.join(
        os.path.dirname(performance_tools.__file__),
        "instructionclass.csv"
    )
)

def ins_index(a, x):
    'Find instruction index'
    i = bisect_left(a, x)
    if i != len(a):
        return i
    else:
        return len(a) - 1

class StaticAnalyzer(object):

    def __init__(self, binary = None):
        self.binary = binary

    def dissassemble_binary(self, binary = None):
        if not binary:
            binary = self.binary

        output = subprocess.check_output("objdump -d {binary}".format(binary = binary),
                                         shell=True).decode()

        # In file:
        # "Disassembly of section .text:"
        return split_and_group(r'Disassembly of section \.(\w+):', output)

    def get_linked_libraries(self, binary = None):
        if not binary:
            binary = self.binary

        od_output = subprocess.check_output("objdump -x {binary}".format(
            binary = binary
        ), shell=True).decode()
        ldd_output = subprocess.check_output("ldd {binary}".format(
            binary = binary
        ), shell=True).decode()

        required_shared_libs = re.findall(r'^\s+NEEDED\s+(\S+)\s*$',
                                          od_output,
                                          re.MULTILINE)

        shared_lib_paths = filter(lambda line: any([re.search(re.escape(lib), line) for lib in required_shared_libs]),
                                  ldd_output.split("\n"))

        shared_lib_paths = [re.search(r"=> (/[^\s]+)", line).group(1) for line in shared_lib_paths]

        return shared_lib_paths

    def __for_all_binaries__(self, x, binary = None, *args):
        if not binary:
            binary = self.binary

        function = getattr(self, x)

        get_req_libs = self.get_linked_libraries(binary)
        get_req_libs.append(binary)

        result = {}

        for b in get_req_libs:
            result.update(function(binary = b, *args))

        return result

    def __count_symbol_instructions__(self, binary = None):
        if not binary:
            binary = self.binary

        elf_section_dict = self.dissassemble_binary(binary)

        # 0000000000400460 <_start>:
        function_dict = split_and_group(r'[0-9A-Fa-f]+ <(\S+)>:', elf_section_dict['text'])
        function_dict = {k: len(list(filter(lambda line: not re.match(r'^\s*$', line),
                                            function.split('\n'))))
                         for k, function in iteritems(function_dict)}
        return function_dict

    def count_symbol_instructions(self, binary = None):
        return self.__for_all_binaries__("__count_symbol_instructions__", binary = binary)

    def __count_total_instructions__(self, binary = None):
        elf_section_dict = self.dissassemble_binary(binary)

        text_section = elf_section_dict['text']
        instructions = list(filter(lambda line: (not re.match(r'^\s*$', line)) and (not re.search(r'[0-9A-Fa-f]+ <\S+>:', line)),
                                   text_section.split("\n")))

        return {os.path.basename(binary): len(instructions)}

    def count_total_instructions(self, binary = None):
        return self.__for_all_binaries__("__count_total_instructions__", binary = binary)

    def symbol_table(self):
        return subprocess.check_output("objdump -t {binary}".format(binary = self.binary), shell=True)

    def generate_report(self):
        report = {
            self.binary: {
                "symbol_instructions": self.count_symbol_instructions(),
                "total_instructions": self.count_total_instructions(),
                "symbol_table": self.symbol_table()
            }
        }

        return report

    def elf_section_sizes(self):
        pass

    def __extract_instructions__(self, binary):

        function_instructions = split_and_group(r'[0-9A-Fa-f]+ <(\S+)>:',
                                                self.dissassemble_binary(binary)['text'])

        r = {}
        for function, instructions in iteritems(function_instructions):
            r[function] = []
            for line in instructions.split("\n"):
                try:
                    # drop garbage info
                    line = line[32:].split()[0]
                    "".upper
                    r[function].append(line.upper())
                except:
                    pass

        return r

    def classify_instructions(self, binary = None):

        if not binary:
            binary = self.binary

        function_instructions = self.__extract_instructions__(binary)

        ins_classes = []
        with open(INS_TABLE, "r") as ins_cls_file:
            for row in csv.reader(ins_cls_file):
                ins_classes.append((row[0], row[1]))

        ins_classes = sorted(ins_classes, key = lambda el: el[0])

        ins_key = [i[0] for i in ins_classes]

        classes = {}

        for function, instructions in iteritems(function_instructions):
            inscounts = {
                "ARITHMETIC": 0,
                "CONTROLFLOW": 0,
                "FLOATINGPOINT": 0,
                "INTMUL": 0,
                "LOAD": 0,
                "STORE": 0,
            }
            for ins in instructions:
                i = ins_index(ins_key, ins)
                if ins.startswith(ins_key[i][:3]):
                    inscounts[ins_classes[i][1]] += 1
                else:
                    try:
                        # We are bisecting by the right, it's likely the
                        # previous instruction is our closest match.
                        predicted = i - 1
                        if ins.startswith(ins_key[predicted][:3]):
                            inscounts[ins_classes[predicted][1]] += 1
                    except:
                        pass

            classes[function] = inscounts

        return classes
