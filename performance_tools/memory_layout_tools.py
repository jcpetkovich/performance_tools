import os

class MemoryLayoutTools(object):
    def pad_memory(self, nbytes):
        os.environ["ENV_PADDING"] = "X" * nbytes
