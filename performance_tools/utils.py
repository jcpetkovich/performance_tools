import re

try:
    from itertools import izip_longest as zip_longest
except ImportError:
    from itertools import zip_longest


def split_and_group(pattern, string):
    pattern_regex = re.compile(pattern)
    result_dict = re.split(pattern_regex, string)[1:]
    result_dict = dict(zip_longest(*[iter(result_dict)] * 2, fillvalue = ""))
    return result_dict
