import subprocess
import os
import shutil
import re
import csv
from six import iteritems
from performance_tools.vcs_project import VCSProject
from performance_tools.utils import split_and_group

class PerfRunner(object):

    def __init__(self):
        self.perf_command = "perf record"
        self.perf_arguments = "-g"
        self.perf_events = "cpu-cycles instructions cache-misses "\
                           "branch-misses page-faults context-switches".split()

    def call_with_perf(self, command):
        """Call the shell command with perf.

        Perf results are placed in perf.data, but should be kept along
        with an associated perf archive file created with
        `Project.archive_perf_results`.

        """
        perf_bin = subprocess.check_output("which perf", shell=True).strip()
        if not os.path.isfile(perf_bin):
            print("No perf binary found, running plain command: {}".format(command))
            return subprocess.check_output(command, shell=True)
        else:
            perf_command = "{perf_command} {perf_arguments} -o perf.data -e {perf_events} {command}".format(
                perf_command = self.perf_command,
                perf_arguments = self.perf_arguments,
                perf_events = ",".join(self.perf_events),
                command = command
            )
            print("Running perf-wrapped command: " + perf_command)
            return subprocess.check_output(perf_command, shell=True)

    def archive_perf_results(self, identifier = None):
        """This will create a perf archive of debug information after
        benchmarking a program.

        """
        if not os.path.isfile("./perf.data"):
            print("Could not find a perf.data file, not saving an archive")
            return

        perf_archive_command = "/usr/libexec/perf-core/perf-archive"
        if not os.path.isfile(perf_archive_command):
            print("Could not find `perf_archive` shell command, trying `perf archive`")
            perf_archive_command = "perf archive"

        subprocess.check_call(perf_archive_command, shell=True)

        if not os.path.isfile('./perf.data.tar.bz2'):
            print("Could not find perf archive file: ./perf.data.tar.bz2 preserving perf.data file anyways")
        else:
            shutil.move("./perf.data.tar.bz2", "./perf.data.{}.tar.bz2".format(identifier))
            shutil.move("./perf.data", "./perf.data.{}".format(identifier))

def read_perf_record_header(output):
    event_count = None
    header = None
    event_regexp = re.compile(r'# Event count \(approx\.\): (\d+)')
    header_regexp = re.compile(r'# [^,]+,')
    header_extract_regexp = re.compile(r'[^#,]+')
    lines = output.split("\n")
    for line in lines:
        event_count = re.search(event_regexp, line)
        if event_count:
            event_count = int(event_count.group(1))
            break

    for line in lines:
        header_line = re.findall(header_regexp, line)
        if header_line:
            header = map(lambda col: col.strip(), re.findall(header_extract_regexp, line))
            break

    return {"header": header, "event_count": event_count}


class PerfAnalyzer(object):

    def __init__(self):
        self.perf_command = "perf report"
        self.perf_arguments = "-g"
        self.perf_seperator = ","

    def csv_output(self):
        PERF_DATA = "perf.data"
        PERF_DATA = os.path.realpath(PERF_DATA)

        command = "{perf_command} {perf_arguments} -t {perf_seperator} -i {perf_data}".format(
            perf_command = self.perf_command,
            perf_arguments = self.perf_arguments,
            perf_seperator = self.perf_seperator,
            perf_data = PERF_DATA
        )
        data = subprocess.check_output(command, shell=True).decode()

        # Samples: 11  of event 'cpu-cycles'
        data = split_and_group(r"# Samples:\s+[\da-zA-Z\.]+\s+of event '(.+)'", data)

        def process(event_data):
            new_event_data = read_perf_record_header(event_data)
            csv_data = filter(lambda line: not line.startswith("#"), event_data.split("\n"))
            csv_data = filter(lambda line: re.search(r'[^,]+,', line), csv_data)
            csv_data = csv.reader(csv_data)
            result = []
            for row in csv_data:
                row = [col.strip() for col in row]
                result.append(row)

            csv_data = result
            new_event_data['symbol_data'] = csv_data
            return new_event_data

        return {event_type: process(event_data) for event_type, event_data in iteritems(data)}
