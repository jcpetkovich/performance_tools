import os
import re
import subprocess

def make_repo(repo):

    def get_repo_type(repo):
        repo_files = os.listdir(repo)
        if any([map(lambda x: x == '.git', repo_files)]):
            return 'git'
        elif any([map(lambda x: x == '.hg', repo_files)]):
            return 'mercurial'
        else:
            return 'none'

    repo_type = get_repo_type(repo)
    if repo_type == 'git':
        return GitRepo(repo)
    elif repo_type == 'mercurial':
        return MercurialRepo(repo)
    elif repo_type == 'none':
        return NoVCSRepo(repo)

class Repo(object):

    def __init__(self, repo_path):
        self.repo = repo_path

    def set_dir(self):
        self._old_dir = os.getcwd()
        os.chdir(self.repo)

    def pop_dir(self):
        os.chdir(self._old_dir)

class GitRepo(Repo):

    def get_commits(self, date_from = None, date_to = None, repo = None):
        repo = repo if repo else self.repo
        date_string =  "{since} {until}".format(
            since = "--since='{}'".format(date_from) if date_from else "",
            until = "--until='{}'".format(date_to) if date_to else ""
        )
        commits = subprocess.check_output("git log {date_string} --no-merges --format='format:%h'".format(
            date_string = date_string
        ), shell=True, cwd=repo).decode()
        return re.split(r'\s+', commits)

    def checkout_commit(self, commit, repo = None):
        repo = repo if repo else self.repo
        subprocess.check_call("git checkout {commitref}".format(commitref=commit), shell=True, cwd=repo)

    def get_current_commit(self, repo = None):
        repo = repo if repo else self.repo
        return subprocess.check_output("git rev-parse --short HEAD", shell=True, cwd=repo).strip().decode()

class NoVCSRepo(Repo):

    def get_commits(self):
        raise NotImplementedError("`get_commits` called on a project "
                                  "without a version control system.")

    def checkout_commit(self):
        raise NotImplementedError("`checkout_commit` called on a project "
                                  "without a version control system.")

    def get_current_commit(self, repo = None):
        raise NotImplementedError("`get_current_commit` called on a project "
                                  "without a version control system.")
class MercurialRepo(Repo):

    def get_commits(self):
        raise NotImplementedError("`get_commits` for MercurialRepo not implemented.")

    def checkout_commit(self):
        raise NotImplementedError("`checkout_commit` for MercurialRepo not implemented.")

    def get_current_commit(self, repo = None):
        raise NotImplementedError("`get_current_commit` for MercurialRepo not implemented.")
