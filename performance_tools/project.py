import subprocess
import os
from performance_tools.repo import NoVCSRepo

class Project(object):

    def __init__(self, path):
        self.path = path
        self.repo = NoVCSRepo(path)
        self.patch_queue = []

    def bench(self):
        self.setup()
        self.run()
        self.collect()
        self.clean()

    def call_in_project(self, f):
        self.repo.set_dir()
        # Run untrusted
        result = None
        try:
            result = f()
        finally:
            self.repo.pop_dir()
        return result

    def call_in_project_sh(self, command):
        print("running: " + command)

        def f():
            return subprocess.check_output(command, shell=True)
        return self.call_in_project(f)

    def __call_in_repo__(self, x, *args):
        self.repo.set_dir()
        try:
            result = getattr(self, x)(*args)
        finally:
            self.repo.pop_dir()
        return result

    def setup(self):
        return self.__call_in_repo__("__setup__")

    def run(self):
        return self.__call_in_repo__("__run__")

    def collect(self):
        return self.__call_in_repo__("__collect__")

    def clean(self):
        return self.__call_in_repo__("__clean__")

    def patch_queue():
        doc = """Patch queue for project, applied before each run."""

        def fget(self):
            return self._patch_queue

        def fset(self, value):
            value = [os.path.realpath(path) for path in value]
            self._patch_queue = value

        def fdel(self):
            del self._patch_queue
        return locals()
    patch_queue = property(**patch_queue())

    def patch(self):
        for patch in self.patch_queue:
            subprocess.check_call("patch -p1 < {patchfile}".format(
                patchfile = patch
            ), shell=True)

    def unpatch(self):
        for patch in reversed(list(self.patch_queue)):
            subprocess.check_call("patch -p1 -R < {patchfile}".format(
                patchfile = patch
            ), shell=True)
