import time

class StopWatch(object):

    def __init__(self, f):
        """Wrap a function, f, so that it stores the elapsed time to call it in
        wrapped_f.elapsed

        """
        self.func = f

    def __call__(self):
        start = time.time()
        result = self.func()
        stop = time.time()
        self.elapsed = stop - start
        return result
