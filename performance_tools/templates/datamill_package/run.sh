#!/usr/bin/env bash

## Clean path, give the minimum to run /etc/profile
# PATH=/usr/bin:/bin
# source /etc/profile
# export PATH

## Use default compiler
# unset CC

# Activate our virtualenv
source dev-python/bin/activate

# Do everything in python.
python run.py
