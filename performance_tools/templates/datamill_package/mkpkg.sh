#!/bin/bash

function die() {
    echo "$1" 1>&2
    exit 1
}

PERFORMANCE_TOOLS_DIR=../../performance_tools/

# Performance tools

rm -rf dev-python
rm -rf src
mkdir src
pushd "${PERFORMANCE_TOOLS_DIR}"
python setup.py sdist || die "Failed to build performance_tools"
popd
cp "${PERFORMANCE_TOOLS_DIR}/dist/"performance_tools-*.tar.gz src

# Package specific actions...
