#!/usr/bin/env python2

# A simple example project...

import shutil
import subprocess
from performance_tools.vcs_project import AutoConfProject
from performance_tools.perf_analyzer import PerfRunner

START_DATE = '2013-09-01'
END_DATE = '2014-09-01'
REPOSITORY = 'x264'
TARGET_FILE = "../shortest_sintel_trailer_2k_480p24.y4m"
OUTPUT_FILE = 'out.log'

class X264(AutoConfProject):

    def __init__(self, repo):
        self.perf_analyzer = PerfRunner()
        super(X264, self).__init__(repo)

    def __run__(self):

        thread_arg = "--threads \$NUM_CPU_CORES"
        command = "./x264 -o /dev/null {thread_arg} {target_file} > {output_file} 2>&1".format(
            thread_arg = "",
            target_file = TARGET_FILE,
            output_file = OUTPUT_FILE
        )
        self.perf_analyzer.call_with_perf(command)

    def __collect__(self):
        self.perf_analyzer.archive_perf_results(self.__git_commit__())
        shutil.move('out.log', 'out.log.{}'.format(self.__get_commit__()))
        shutil.move('x264', 'x264.{}'.format(self.__get_commit__()))


x264 = X264(REPOSITORY)
x264.configure_flags = ['--enable-debug']
x264.perf_events = ['instructions', 'cpu-cycles', 'cache-misses']

commits_to_run = x264.get_commits(START_DATE, END_DATE)

for commit in commits_to_run:
    x264.bench_commit(commit)
