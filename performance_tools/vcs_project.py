import subprocess
from performance_tools.project import Project
from performance_tools.repo import make_repo

class VCSProject(Project):
    def __init__(self, repo):
        super(VCSProject, self).__init__(repo)
        self.repo = make_repo(repo)

    def bench_commit(self, commit):
        self.checkout_commit(commit)
        self.bench()

    def __get_commit__(self):
        return subprocess.check_output("git rev-parse --short HEAD", shell=True).strip().decode()

    def commit():
        doc = """The current commit of the project"""

        def fget(self):
            return self.__call_in_repo__('__get_commit__')

        return locals()
    commit = property(**commit())

    def get_commits(self, date_from = None, date_to = None):
        return self.repo.get_commits(date_from, date_to)

    def checkout_commit(self, commit):
        return self.repo.checkout_commit(commit)

    def get_current_commit(self):
        return self.repo.get_current_commit()

class AutoConfProject(VCSProject):

    def __init__(self, path):
        self.configure_flags = []
        super(AutoConfProject, self).__init__(path)

    def configure(self):
        subprocess.check_call("./configure {}".format(
            " ".join(self.configure_flags)
        ), shell=True)

    def make(self):
        subprocess.check_call("make", shell=True)

    def __setup__(self):
        self.patch()
        self.configure()
        self.make()

    def __clean__(self):
        subprocess.check_call("make clean", shell=True)
        self.unpatch()
